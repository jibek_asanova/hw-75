const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());
const port = 8000;

app.post('/encode', (req, res) => {
  if(!req.body.message) {
    return res.status(400).send({error: 'data not valid'})
  }
  const encodeMessage = {
    encoded:  Vigenere.Cipher(req.body.password).crypt(req.body.message)
  }
  res.send(encodeMessage);
});


app.post('/decode', (req, res) => {
  if(!req.body.message) {
    return res.status(400).send({error: 'data not valid'})
  }
  const encodeMessage = {
    decoded:  Vigenere.Decipher(req.body.password).crypt(req.body.message)
  }
  res.send(encodeMessage);
});

app.listen(port, () => {
  console.log('Server started on', port);
})
