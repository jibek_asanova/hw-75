import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {decodeMessage, encodeMessage} from "../store/actions/passwordsActions";

const useStyles = makeStyles(theme => ({
    textField: {
        margin: theme.spacing(2)
    }
}));


const Form = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const encodedMessageF = useSelector(state => state.encodedMessage);
    const decodedMessageF = useSelector(state => state.decodedMessage);

    const [state, setState] = useState({
        password: '',
        encode: '',
        decode: ''
    });

    const changeValue = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const decodeMessageFunction = async (e) => {
        e.preventDefault();
        await dispatch(decodeMessage({
            message: state.decode,
            password: state.password
        }));

        setState(prev => ({
            ...prev,
            encode: decodedMessageF.decoded
        }));
    };

    const encodeMessageFunction = async (e) => {
        e.preventDefault();
        await dispatch(encodeMessage({
            message: state.encode,
            password: state.password
        }));

        setState(prev => ({
            ...prev,
            decode: encodedMessageF.encoded
        }));
    }

    return (
        <div>
            <Grid container direction="column" spacing={2}>
                <Grid item>
                    <TextField
                        placeholder="Decoded message"
                        multiline
                        name="decode"
                        value={state.decode}
                        onChange={changeValue}
                        className={classes.textField}
                    />
                </Grid>
                <Grid item container>
                    <TextField
                        id="outlined-basic"
                        label="Password"
                        variant="outlined"
                        name="password"
                        value={state.password}
                        onChange={changeValue}
                    />
                    <Button color="primary" onClick={decodeMessageFunction}>decode</Button>
                    <Button color="primary" onClick={encodeMessageFunction}>encode</Button>
                </Grid>
                <Grid item>
                    <TextField
                        placeholder="Encoded message"
                        multiline
                        name="encode"
                        value={state.encode}
                        onChange={changeValue}
                        className={classes.textField}
                    />
                </Grid>
            </Grid>
        </div>
    );
};
export default Form;