import axios from "axios";

export const ENCODE_MESSAGE_SUCCESS = 'ENCODE_MESSAGE_SUCCESS';
export const DECODE_MESSAGE_SUCCESS = 'DECODE_MESSAGE_SUCCESS';

export const encodeMessageSuccess = message => ({type: ENCODE_MESSAGE_SUCCESS, payload: message});
export const decodeMessageSuccess = message => ({type: DECODE_MESSAGE_SUCCESS, payload: message});


export const decodeMessage = (message) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://127.0.0.1:8000/decode', message);
            dispatch(decodeMessageSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    }
};

export const encodeMessage = (message) => {
    return async dispatch => {
        try {
            const response = await axios.post('http://127.0.0.1:8000/encode', message);
            dispatch(encodeMessageSuccess(response.data))
        } catch (e) {
            console.log(e);
        }
    }
};

