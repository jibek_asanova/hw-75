import {DECODE_MESSAGE_SUCCESS, ENCODE_MESSAGE_SUCCESS} from "../actions/passwordsActions";

const initialState = {
    encodedMessage: {
        encoded: ''
    },
    decodedMessage: {
        decoded: ''
    }

}

const passwordReducer = (state = initialState, action) => {
    switch (action.type) {
        case ENCODE_MESSAGE_SUCCESS:
            return {...state, encodedMessage: action.payload}
        case DECODE_MESSAGE_SUCCESS:
            return {...state, decodedMessage: action.payload}
        default:
            return state;
    }
}

export default passwordReducer;